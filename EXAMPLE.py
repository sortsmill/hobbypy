# Copyright (C) 2015 Khaled Hosny and Barry Schwartz
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

from hobbypy import (cycle, controls,
                     tension, tension_atleast,
                     direction, angle, curl,
                     left, up, right, down,
                     oo, ooo, xx, xxx,
                     hobby_guide_to_array,
                     array_to_hobby_guide,
                     solve_hobby_guide,
                     hobby_guide_is_cyclic,
                     hobby_guide_points)

guide = [(0, 0), oo, (1, 1), ooo, (2, -4), xx, (0, -5),
         xxx, (-1, -5), up, oo, -2-5j, down, oo, left, -3-5j,
         oo, right, (-3, -6), angle (150),
         tension (0.75), (-4, -4), direction (1, 1),
         tension (5, 0.75), (-3, -3), direction ((1, 1)),
         tension_atleast (2), (-2, -2), direction (1+1j),
         tension_atleast (2, 3), curl (2), cycle]

print (guide)
print ("")
print (array_to_hobby_guide (hobby_guide_to_array (guide)))
print ("")
print (solve_hobby_guide (guide))
print ("")
print ("cyclic? " + str (hobby_guide_is_cyclic (guide)))
print ("")
for p in hobby_guide_points (guide):
  print (p)

print ("")
circle1 = ((-1, 0), up, oo, (0, 1), right, oo,
           (1, 0), down, oo, (0, -1), left, oo, cycle)
print ("circle1:")
print (array_to_hobby_guide (hobby_guide_to_array (circle1)))
print (solve_hobby_guide (circle1))
print (list (hobby_guide_points (circle1)))
print ("")
circle2 = ((-1, 0), ooo, (0, 1), ooo,
           (1, 0), ooo, (0, -1), ooo, cycle)
print ("circle2:")
print (array_to_hobby_guide (hobby_guide_to_array (circle2)))
print (solve_hobby_guide (circle2))
print (list (hobby_guide_points (circle2)))

print ("")
square = ((-1, 0), xx, (0, 1), xx,
           (1, 0), xx, (0, -1), xx, cycle)
print ("square:")
print (array_to_hobby_guide (hobby_guide_to_array (square)))
print (solve_hobby_guide (square))
print (list (hobby_guide_points (square)))
print ("")
almost_square = ((-1, 0), xxx, (0, 1), xxx,
                 (1, 0), xxx, (0, -1), xxx, cycle)
print ("almost_square:")
print (array_to_hobby_guide (hobby_guide_to_array (almost_square)))
print (solve_hobby_guide (almost_square))
print (list (hobby_guide_points (almost_square)))
