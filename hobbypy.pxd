# -*- coding: utf-8 -*-

# Copyright (C) 2015 Khaled Hosny and Barry Schwartz
#
# This file is part of Hobbypy.
# 
# Hobbypy is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# Hobbypy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

cdef extern from "sortsmill/core.h":
  void solve_hobby_guide_tokens (size_t n_input, const double *input_tokens,
                                 size_t *n_output, double *output_tokens,
                                 int *info, const char **info_message,
                                 size_t *bad_token_index)
