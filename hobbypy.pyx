# -*- coding: utf-8 -*-

# Copyright (C) 2015 Khaled Hosny and Barry Schwartz
#
# This file is part of Hobbypy.
# 
# Hobbypy is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# Hobbypy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

from __future__ import division

from cpython cimport array as c_array
from array import array
import math

HOBBY_GUIDE_POINT = 0
HOBBY_GUIDE_CYCLE = 1
HOBBY_GUIDE_TENSION = 2
HOBBY_GUIDE_ATLEAST = 3
HOBBY_GUIDE_DIR = 4
HOBBY_GUIDE_CURL = 5
HOBBY_GUIDE_CTRL = 6

cdef int HOBBY_GUIDE_TOKEN_ERROR = -1
cdef int HOBBY_GUIDE_SOLUTION_NOT_FOUND = -2

class hobbypy_error (Exception):
  """Base class for exceptions in Hobbypy."""
  pass

class hobby_guide_internal_error (hobbypy_error):
  """Exception hinting that something is wrong in Sorts Mill Core."""
  pass

class hobby_guide_error (hobbypy_error):
  """Exception for errors in a Hobby guide."""

  def __init__ (self, message, position = None):
    self.message = message
    self.position = position

  def __repr__ (self):
    return ("hobby_guide_error (" + repr (self.message) +
            ", " + repr (self.message) +
            ", " + repr (self.position) + ")")

class hobby_guide_token_error (hobbypy_error):
  """Exception corresponding to the info code HOBBY_GUIDE_TOKEN_ERROR."""

  def __init__ (self, info_message, tokens, bad_token_index):
    self.info_message = info_message
    self.tokens = tokens
    self.bad_token_index = bad_token_index

  def __repr__ (self):
    return ("hobby_guide_token_error (" + repr (self.info_message) +
            ", " + repr (self.tokens) +
            ", " + repr (self.bad_token_index) + ")")

class hobby_guide_solution_not_found (hobbypy_error):
  """Exception corresponding to the info code HOBBY_GUIDE_SOLUTION_NOT_FOUND."""

  def __init__ (self, info_message, tokens):
    self.info_message = info_message
    self.tokens = tokens
    self.bad_token_index = None

  def __repr__ (self):
    return ("hobby_guide_solution_not_found (" + repr (self.tokens) +
            ", " + repr (self.info_message) + ")")

class cycle:

  def __init__ (self):
    pass

  def __repr__ (self):
    return "cycle ()"

class controls:

  def __init__ (self, x1, y1, x2 = None, y2 = None):
    if x2 is None and y2 is None:
      try:
        self.x1 = x1[0]
        self.y1 = x1[1]
      except:
        self.x1 = x1.real
        self.y1 = x1.imag
      try:
        self.x2 = y1[0]
        self.y2 = y1[1]
      except:
        self.x2 = y1.real
        self.y2 = y1.imag
    else:
      self.x1 = x1
      self.y1 = y1
      self.x2 = x2
      self.y2 = y2

  def __repr__ (self):
    return ("controls ((" + repr (self.x1) + ", " + repr (self.y1) +
            "), (" + repr (self.x2) + ", " + repr (self.y2) + "))")

  def pairs (self):
    return [(self.x1, self.y1), (self.x2, self.y2)]

  def cmplx (self):
    return [complex (self.x1, self.y1), complex (self.x2, self.y2)]

class tension:

  def __init__ (self, left_tension, right_tension = None, enforce_bounds = True):
    if right_tension is None:
      right_tension = left_tension
    if enforce_bounds:
      if left_tension < 0.75 or right_tension < 0.75:
        raise hobby_guide_error ("Tension less than 0.75")
    self.left_tension = left_tension
    self.right_tension = right_tension

  def __repr__ (self):
    if self.left_tension == self.right_tension:
      s = "tension (" + repr (self.left_tension) + ")"
    else:
      s = ("tension (" + repr (self.left_tension) +
           ", " + repr (self.right_tension) + ")")
    return s

class tension_atleast:

  def __init__ (self, left_tension, right_tension = None,
                bint enforce_bounds = True):
    if right_tension is None:
      right_tension = left_tension
    if enforce_bounds:
      if left_tension < 0.75 or right_tension < 0.75:
        raise hobby_guide_error ("Tension less than 0.75")
    self.left_tension = left_tension
    self.right_tension = right_tension

  def __repr__ (self):
    if self.left_tension == self.right_tension:
      s = "tension_atleast (" + repr (self.left_tension) + ")"
    else:
      s = ("tension_atleast (" + repr (self.left_tension) +
           ", " + repr (self.right_tension) + ")")
    return s

class curl:

  def __init__ (self, curl_amount, bint enforce_bounds = True):
    if enforce_bounds:
      if curl_amount < 0:
        raise hobby_guide_error ("curl less than zero")
    self.curl_amount = curl_amount

  def __repr__ (self):
    return "curl (" + repr (self.curl_amount) + ")"

class direction:

  def __init__ (self, x, y = None):
    if y is None:
      try:
        self.x = x[0]
        self.y = x[1]
      except:
        self.x = x.real
        self.y = x.imag
    else:
      self.x = x
      self.y = y

  def __repr__ (self):
    return "direction ((" + repr (self.x) + ", " + repr (self.y) + "))"

  def pair (self):
    return (self.x, self.y)

  def cmplx (self):
    return complex (self.x, self.y)

  def angle (self):
    return math.degrees (math.atan2 (self.y, self.x))

def angle (phi):
  """Return the direction corresponding to an angle of phi degrees."""
  phi = math.radians (phi)
  return direction (math.cos (phi), math.sin (phi))

# Equivalents of Metafont’s .. ... -- ---
#
# Note that Metafont implements -- differently: being unable to use an
# infinite tension, Metafont instead surrounds a finite tension with
# two curl specifications. An infinite tension is more convenient for
# an implementation that employs Python sequences rather than Metafont
# macros.
oo = tension (1.0)
ooo = tension_atleast (1.0)
xx = tension (float ('inf'))
xxx = tension (4095.99998) # This number is called ‘infinity’ in plain.mf.

left = direction (-1.0, 0.0)
up = direction (0.0, 1.0)
right = direction (1.0, 0.0)
down = direction (0.0, -1.0)

cdef c_array.array double_array_template = array ('d', [])

cdef size_t SIZE_MAX = 0
SIZE_MAX = ~SIZE_MAX

def hobby_guide_to_array (guide):
  cdef size_t size = 0
  for entry in guide:
    if isinstance (entry, controls):
      if SIZE_MAX - 6 < size:
        raise hobby_guide_error ("Hobby guide too long", size - 1)
      size += 6
    else:
      if SIZE_MAX - 3 < size:
        raise hobby_guide_error ("Hobby guide too long", size - 1)
      size += 3
  cdef c_array.array darray = (
    c_array.clone (double_array_template, size, zero = False)
  )
  cdef double *A = darray.data.as_doubles
  cdef size_t i = 0
  cdef size_t position = 0
  for entry in guide:
    if entry == cycle or isinstance (entry, cycle):
      A[i] = HOBBY_GUIDE_CYCLE
      A[i + 1] = 0.0
      A[i + 2] = 0.0
      i += 3
    elif isinstance (entry, controls):
      A[i] = HOBBY_GUIDE_CTRL
      A[i + 1] = entry.x1
      A[i + 2] = entry.y1
      A[i + 3] = HOBBY_GUIDE_CTRL
      A[i + 4] = entry.x2
      A[i + 5] = entry.y2
      i += 6
    elif isinstance (entry, tension):
      A[i] = HOBBY_GUIDE_TENSION
      A[i + 1] = entry.left_tension
      A[i + 2] = entry.right_tension
      i += 3
    elif isinstance (entry, tension_atleast):
      A[i] = HOBBY_GUIDE_ATLEAST
      A[i + 1] = entry.left_tension
      A[i + 2] = entry.right_tension
      i += 3
    elif isinstance (entry, direction):
      A[i] = HOBBY_GUIDE_DIR
      A[i + 1] = entry.x
      A[i + 2] = entry.y
      i += 3
    elif isinstance (entry, curl):
      A[i] = HOBBY_GUIDE_CURL
      A[i + 1] = entry.curl_amount
      A[i + 2] = 0.0
      i += 3
    elif isinstance (entry, complex):
      A[i] = HOBBY_GUIDE_POINT
      A[i + 1] = entry.real
      A[i + 2] = entry.imag
      i += 3
    elif len (entry) == 2:
      A[i] = HOBBY_GUIDE_POINT
      A[i + 1] = entry[0]
      A[i + 2] = entry[1]
      i += 3
    else:
      raise hobby_guide_error ("unexpected object in Hobby guide",
                               position)
    position += 1
  return darray

def array_to_hobby_guide (c_array.array darray):
  cdef size_t n = len (darray)
  if n % 3 != 0:
    message = "token array is truncated prematurely"
    raise hobby_guide_token_error (message, darray, n // 3)
  cdef double *A = darray.data.as_doubles
  guide = []
  cdef size_t i = 0
  while i < n:
    if A[i] == HOBBY_GUIDE_POINT:
      guide.append ((A[i + 1], A[i + 2]))
      i += 3
    elif A[i] == HOBBY_GUIDE_CYCLE:
      guide.append (cycle ())
      i += 3
    elif A[i] == HOBBY_GUIDE_TENSION:
      guide.append (tension (A[i + 1], A[i + 2]))
      i += 3
    elif A[i] == HOBBY_GUIDE_ATLEAST:
      guide.append (tension_atleast (A[i + 1], A[i + 2]))
      i += 3
    elif A[i] == HOBBY_GUIDE_DIR:
      guide.append (direction (A[i + 1], A[i + 2]))
      i += 3
    elif A[i] == HOBBY_GUIDE_CURL:
      guide.append (curl (A[i + 1]))
      i += 3
    elif A[i] == HOBBY_GUIDE_CTRL:
      if n - 3 <= i:
        message = "HOBBY_GUIDE_CTRL appears in other than a pair"
        raise hobby_guide_token_error (message, darray, i // 3)
      guide.append (controls (A[i + 1], A[i + 2], A[i + 4], A[i + 5]))
      i += 6
  return guide

def hobby_solve_array (c_array.array darray):
  """Solve a token array, using the Hobby algorithms support in
  Sorts Mill Core.

  """
  cdef size_t n_input = len (darray) // 3
  cdef size_t n_output
  cdef const double *input_tokens = darray.data.as_doubles
  cdef c_array.array output_darray = (
    c_array.clone (double_array_template, (9 * n_input) // 2, zero = False)
  )
  cdef double *output_tokens = output_darray.data.as_doubles
  cdef int info
  cdef const char *info_message
  cdef size_t bad_token_index
  solve_hobby_guide_tokens (n_input, input_tokens,
                            &n_output, output_tokens,
                            &info, &info_message, &bad_token_index)
  if info == HOBBY_GUIDE_TOKEN_ERROR:
    raise hobby_guide_token_error (info_message, darray, bad_token_index)
  elif info == HOBBY_GUIDE_SOLUTION_NOT_FOUND:
    raise hobby_guide_solution_not_found (info_message, darray)
  elif info != 0:
    raise hobby_guide_internal_error ()

  c_array.resize (output_darray, 3 * n_output)
  return output_darray

def solve_hobby_guide (guide):
  """FIXME: Document this."""

  cdef c_array.array input_darray = hobby_guide_to_array (guide)

  # FIXME: Catch hobby_guide_token_error exceptions raised by
  # hobby_solve_array, and convert them to hobby_guide_error
  # exceptions. Something like this is done in Sorts Mill Core Guile.
  cdef c_array.array output_darray = hobby_solve_array (input_darray)
  
  return array_to_hobby_guide (output_darray)

#-----------------------------------------------------------------------
#
# Functions for use with FontForge’s Python interface.
#

def hobby_guide_is_cyclic (guide):
  """Does the given Hobby guide cycle? (In FontForge one would ask,
  "Is the contour closed?)"

  """
  n = len (guide)
  return 0 < n and (guide[n - 1] == cycle or
                    isinstance (guide[n - 1], cycle))

def hobby_guide_points (guide):
  """FIXME: Document this."""
  solution = solve_hobby_guide (guide)
  for entry in solution:
    if isinstance (entry, controls):
      # Off-curve points.
      yield (entry.x1, entry.y1, False)
      yield (entry.x2, entry.y2, False)
    elif isinstance (entry, cycle):
      pass
    else:
      # On-curve point.
      yield (entry[0], entry[1], True)

#-----------------------------------------------------------------------
