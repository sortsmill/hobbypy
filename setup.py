# -*- coding: utf-8 -*-

# Copyright (C) 2015 Khaled Hosny and Barry Schwartz
#
# This file is part of Hobbypy.
# 
# Hobbypy is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# Hobbypy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

#-----------------------------------------------------------------------
#
# If you have Sorts Mill Core installed outside the usual places, for
# instance in ${HOME}, commands such as
#
#   CFLAGS="-I${HOME}/include" LDFLAGS="-L${HOME}/lib64" python setup.py build
#
# should work.
#
#-----------------------------------------------------------------------

from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

setup (
  name = "hobbypy",
  version = "1.0.0",
  description = "Interface to the Hobby algorithm support in Sorts Mill Core",
  author = "Sorts Mill",
  author_email = "sortsmill@crudfactory.com",
  url = "https://bitbucket.org/sortsmill/hobbypy",
  ext_modules = cythonize ([Extension ("hobbypy", ["hobbypy.pyx"],
                                       libraries = ["sortsmill-core"])])
)

#-----------------------------------------------------------------------
